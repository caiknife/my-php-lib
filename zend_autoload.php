<?php
require_once 'kint/Kint.class.php';
if (version_compare(PHP_VERSION, '5.3', '<')) {
    require_once dirname(__FILE__) . '/zf1_autoload.php';
} else {
    require_once __DIR__ . '/zf2_autoload.php';
}