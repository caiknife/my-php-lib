my-php-lib
==========

My PHP Library

* Zend Framework 1
* Zend Framework 2
* Simple Test
* Kint
* YAF
* PHPExcel

## Usage for autoloading

``` php
<?php
require_once 'zend_autoload.php';
```

Then ZF1, ZF2 and Kint will be autoloaded in context. But if PHP version is under 5.3, only ZF1 and Kint will be autoloaded.

If you want to use PHPExcel, use these code below:

```php
<?php
require_once 'PHPExcel/Classes/PHPExcel.php';
```

Enjoy!
