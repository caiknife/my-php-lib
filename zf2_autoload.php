<?php
require_once __DIR__ . '/Zend2/Loader/StandardAutoloader.php';
use Zend\Loader\StandardAutoloader;
$loader = new StandardAutoloader(array(
    StandardAutoloader::AUTOREGISTER_ZF => true,
    StandardAutoloader::ACT_AS_FALLBACK => true,
));
$loader->register();
